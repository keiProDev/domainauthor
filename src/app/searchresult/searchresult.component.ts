import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {MdDialog, MdDialogRef, MdDialogConfig, MD_DIALOG_DATA} from '@angular/material';
import { SettingmodalComponent } from '../settingmodal/settingmodal.component';
import { DataService } from '../data.service';
import 'rxjs/add/operator/switchMap';
@Component({
  selector: 'app-searchresult',
  templateUrl: './searchresult.component.html',
  styleUrls: ['./searchresult.component.css']
})
export class SearchresultComponent implements OnInit {

  constructor(public dialog: MdDialog,private route: ActivatedRoute,private router: Router,public data:DataService) { }
  exts = [];
  ngOnInit() {
  }
  openSettingDialog() {
    console.log("open dialog");
    let psDialogRef = this.dialog.open(SettingmodalComponent, {
      disableClose: false,
      hasBackdrop: true,
      panelClass: 'overay-pan-background',
      backdropClass: 'custom-backdrop-class',
      width: '460px'
    });
  }

}
