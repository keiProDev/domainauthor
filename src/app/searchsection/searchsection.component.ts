import { Component, OnInit } from '@angular/core';
import {MdDialog, MdDialogRef, MdDialogConfig, MD_DIALOG_DATA} from '@angular/material';
import { SettingmodalComponent } from '../settingmodal/settingmodal.component';
import { DataService } from '../data.service';
@Component({
  selector: 'app-searchsection',
  templateUrl: './searchsection.component.html',
  styleUrls: ['./searchsection.component.css']
})
export class SearchsectionComponent implements OnInit {

  constructor(public dialog: MdDialog,public data:DataService) { }
    keyword = "";
    ngOnInit() {
      this.keyword = this.data.keyword;
    }
    
    openSettingDialog() {
      console.log("open dialog");
      let psDialogRef = this.dialog.open(SettingmodalComponent, {
        disableClose: false,
        hasBackdrop: true,
        panelClass: 'overay-pan-background',
        backdropClass: 'custom-backdrop-class',
        width: '460px'
      });
    }
    search(){
      this.data.setKeyword(this.keyword);
    }

}
