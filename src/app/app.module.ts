import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routes';
import { MdDialogModule, MdButtonModule ,MdCheckboxModule ,MdSelectModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { SearchsectionComponent } from './searchsection/searchsection.component';
import { HomepageComponent } from './homepage/homepage.component';
import { SearchresultComponent } from './searchresult/searchresult.component';
import { MoreresultComponent } from './moreresult/moreresult.component';
import { MainsearchComponent } from './mainsearch/mainsearch.component';
import { SettingmodalComponent } from './settingmodal/settingmodal.component';
import { DataService } from './data.service'
@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    SearchsectionComponent,
    HomepageComponent,
    SearchresultComponent,
    MoreresultComponent,
    MainsearchComponent,
    SettingmodalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    BrowserAnimationsModule,
    MdDialogModule,
    MdButtonModule,
    MdCheckboxModule,
    MdSelectModule
  ],
  exports: [
    SettingmodalComponent
  ],
  entryComponents: [
    SettingmodalComponent
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
