import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
@Component({
  selector: 'app-settingmodal',
  templateUrl: './settingmodal.component.html',
  styleUrls: ['./settingmodal.component.css']
})
export class SettingmodalComponent implements OnInit {

  constructor(public data:DataService) { }
  hypens = false;
  newExt :string = "";
  newExtError = false;
  addmore_show = false;
  selectedValue: string = "GoDaddy";
  services = [];
  exts = [];
  public validate = "form-control more-input";
  ngOnInit() {
    let temp = this.data.exts.map(x => Object.assign({}, x));
    this.exts = temp;
    this.services = this.data.services;
    this.hypens = this.data.hypens;
  }
  addmore(){
    this.addmore_show = !this.addmore_show;
  }
  Cancel(){
    let temp = this.data.exts.map(x => Object.assign({}, x));
    this.exts = temp;
  }
  ApplySuccess(){
    console.log(this.exts);
    let temp = this.exts.map(x => Object.assign({}, x));
    this.data.exts = temp;
    this.data.hypens = this.hypens;
    this.data.refreshKeyword();
  }
  checkError(){
    if (this.newExtError == true){
      this.validate = "form-control more-input error"
    }else{
      this.validate = "form-control more-input"
    }
  }
  onEnter(){
    if (this.newExt!="" && this.newExt.substring(0,1)=="."){
      this.newExtError = false;
      var temp = {
        name:this.newExt,value:false
      };
      this.exts.push(temp);
      this.newExt = "";
    }else{
      this.newExtError = true;
    }
    this.checkError();
  }
}
