import { Injectable } from '@angular/core';

@Injectable()
export class DataService {
  services = ["GoDaddy","Bluehost","Namecheap"];
  prefix = ["My","The","Mr","Ultimate","Ilove","Smart","Easy"];
  suffix = ["Head","Hero","King","Queen","Digital","Facts","Paradise"];
  exts = [
    {name:".com",value:true},
    {name:".net",value:true},
    {name:".org",value:true},
    {name:".info",value:true},
    {name:".co",value:false},
    {name:".io",value:false},
    {name:".pro",value:false},
    {name:".me",value:false},
    {name:".au",value:false},
    {name:".zp",value:false},
    {name:".us",value:false},
    {name:".er",value:false}
  ];
  hypens = false;
  keyword = "";
  domainKeyword = "";
  constructor() {
    this.keyword = ""
  }
  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min - 1)) + min;
  }
  getUppercase(value){
    return value.charAt(0).toUpperCase()+value.substr(1).toLowerCase();
  }
  setKeyword(value){
    this.keyword = value;
    if (this.hypens==false){
      this.domainKeyword = this.prefix[this.getRandomInt(0,this.prefix.length)] + this.getUppercase(value) + this.suffix[this.getRandomInt(0,this.suffix.length)];
    }else{
      this.domainKeyword = this.prefix[this.getRandomInt(0,this.prefix.length)] + "-" +this.getUppercase(value)+"-" + this.suffix[this.getRandomInt(0,this.suffix.length)];
    }
  }
  refreshKeyword(){
    if (this.hypens==false){
      this.domainKeyword = this.prefix[this.getRandomInt(0,this.prefix.length)] + this.getUppercase(this.keyword) + this.suffix[this.getRandomInt(0,this.suffix.length)];
    }else{
      this.domainKeyword = this.prefix[this.getRandomInt(0,this.prefix.length)] + "-" +this.getUppercase(this.keyword)+"-" + this.suffix[this.getRandomInt(0,this.suffix.length)];
    }
  }
  getExts(){
    return this.exts;
  }
  setExts(value){
    this.keyword = value;
  }
}
