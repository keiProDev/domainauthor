import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DataService } from '../data.service';
import {MdDialog, MdDialogRef, MdDialogConfig, MD_DIALOG_DATA} from '@angular/material';
import { SettingmodalComponent } from '../settingmodal/settingmodal.component';
@Component({
  selector: 'app-mainsearch',
  templateUrl: './mainsearch.component.html',
  styleUrls: ['./mainsearch.component.css']
})
export class MainsearchComponent implements OnInit {

  constructor(public dialog: MdDialog,public data:DataService,private router: Router) { }
  keyword = "";
  ngOnInit() {
  }
  openSettingDialog() {
    console.log("open dialog");
    let psDialogRef = this.dialog.open(SettingmodalComponent, {
      disableClose: false,
      hasBackdrop: true,
      panelClass: 'overay-pan-background',
      backdropClass: 'custom-backdrop-class',
      width: '460px'
    });
  }
  search(){
    if (this.keyword!==""){
      this.data.setKeyword(this.keyword);
      this.router.navigate(["/result"]);
    }
  }
}
