import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { SearchresultComponent } from './searchresult/searchresult.component';
import { MoreresultComponent } from './moreresult/moreresult.component';
const appRoutes: Routes = [{
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: HomepageComponent
    },
    {
        path: 'result',
        component: SearchresultComponent
    },
    {
        path: 'more',
        component: MoreresultComponent
    }

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);