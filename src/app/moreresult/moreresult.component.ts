import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
@Component({
  selector: 'app-moreresult',
  templateUrl: './moreresult.component.html',
  styleUrls: ['./moreresult.component.css']
})
export class MoreresultComponent implements OnInit {

  constructor(public data:DataService) { }
  currentIndex = 1;
  keywords = ["WEBSITE","BUSINESS","SHOP","EBOOK","SOFTWARE","APP","NEWS","COMMUNITY","VIDEO","MARKETPLACE","DEALS","REVIEWS"];
  ngOnInit() {

  }
  previous(){
    if (this.currentIndex>0)
    this.currentIndex--;
  }
  next(){
    if (this.currentIndex<this.keywords.length-1)
    this.currentIndex++;
  }

}
